# Overview
This project aims to allow developers to deploy the cardano stack (Cardano Node + Cardano DB Sync) on kubernetes clusters.

# Repositiories
- Gitlab: https://gitlab.com/cardano-tools/cardano-kstack
- artifacthub: https://artifacthub.io/packages/helm/cardano-kstack/cardano-kstack

---

# Requirements
- Helm 3 properly installed and configured to point to a working kubernetes cluster
- Enough cpu, memory and storage available on the cluster to allocate the resources configured on [[values.yml]]
- Have at least 1 available node on your cluster which can allocate 8Gi of memory for a single pod
- All testing during development was made on GCP GKE clusters, compatibility with other kubernetes solutions is expected, but not guaranteed at this point

# Usage

### Installing from the remote repository

```
helm repo add cardano-kstack  https://cardano-tools.gitlab.io/cardano-kstack/
helm repo update
helm install yourReleaseName cardano-kstack/cardano-kstack -n yourNamespace
```


### Installing from a local clone of this repository

`helm install -n yourNamespace -f Chart.yaml yourReleaseName ./ --create-namespace`

### Uninstalling
`helm uninstall -n yourNamespace yourReleaseName`

In order to guarantee fault tolerance all applications are deployed as part of stateful sets. As a collateral volumes are persisted even if you uninstall a given release (to prevent data loss). You might need to do manual cleanup after an uninstall.

### Configuration
All configurable parameters can be seen on the `values.yaml` file. You can override parameters either by passing your own `values.yaml` file or by overriding on the command line. e.g.:

`helm install yourReleaseName cardano-kstack/cardano-kstack -n yourNamespace --set network=mainnet`

`helm install -f myValuesFile.yaml yourReleaseName cardano-kstack/cardano-kstack -n yourNamespace`

# Prometheus Monitoring

To enable Prometheus monitoring you need to provide a custom built cardano-node container image with a configuration file that properly binds the Prometheus service to the kubernetes network interface. In practice this means making sure that you have this configuration on your cardano-node `config.json` file:
```
"hasPrometheus": [
  "0.0.0.0",
  12798
]
```
For more detailed information check https://github.com/input-output-hk/cardano-node/blob/master/doc/logging-monitoring/prometheus.md

Under the `examples/prometheus` path of the git repository you can find the necessary files to run a working example (as of cardano-node 1.33.0) of a mainnet cardano-node with prometheus monitoring enabled.    

### Running the example

1. Build the Dockerfile provided on `examples/prometheus/docker/`
1. Push the built image to a repository to which your kubernetes cluster has access
1. Put the path to your image on the `nodeImage` property of the `examples/prometheus/values.yaml` file
1. Install the chart either from the artifacthub repository or from a local clone of this repository passing the values.yaml file edited above:
  * artifacthub: `helm install kstack-prometheus cardano-kstack/cardano-kstack -n example -f examples/prometheus/values.yaml --create-namespace`
  * local: `helm install -n example -f examples/prometheus/values.yaml -f Chart.yaml kstack-prometheus ./ --create-namespace`
1. If you did all steps successfully prometheus metrics should be available as a service on your kubernetes cluster namespace. You can access the metrics on local by running `kubectl -n example port-forward kstack-prometheus-cardano-kstack-mainnet-0  12798`, this will make the metrics accessible on your local machine on `http://localhost:12798/metrics`

---

# Components

![Cardano Stack High Level Architecture](docs/cardano_kstack_high_level_architecture.png)

---

# Notes

This is provided for educational purposes. This code should not be considered production ready. More information see the LICENSE file.

My main goal with this project is to help myself and other developers to be able to setup a quick production-like environment. I hope that this lowers Cardano's barrier of entry and invites more people to experiment with the platform. Donations are never necessary, but always welcomed:

♡ ADA addr1qx5tuj3vx4j2903z76guh9pj56gxc9vw7m6l2jzgmg6rwca5f9eqmt29y3wggprtdjx8nd64klz39hhr20an78tp3rvs2xr938
